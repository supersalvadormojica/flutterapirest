import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_api_rest/API/authentication_api.dart';
import 'package:flutter_api_rest/Utils/dialogs.dart';
import 'package:flutter_api_rest/Utils/responsive.dart';
import 'package:flutter_api_rest/data/authentication_client.dart';
import 'package:flutter_api_rest/pages/home_page.dart';
import 'package:flutter_api_rest/widgets/input_text.dart';
import 'package:get_it/get_it.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({super.key});

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _authenticationAPI = GetIt.instance<AuthenticationAPI>();
  final _authenticationClient = GetIt.instance<AuthenticationClient>();
  final GlobalKey<FormState> _formKey = GlobalKey();
  String _email='', _password='';
 
 Future<void> _submit() async {
    final bool? isOk = _formKey.currentState?.validate();
    ProgressDialog.show(context);
    if(isOk!){
     final response = await _authenticationAPI.login(email: _email, password: _password);
    ProgressDialog.dissmiss(context);
    if(response.data != null){
          await _authenticationClient.saveSession(response.data);
          Navigator.pushNamedAndRemoveUntil(context, HomePage.routeName, (_)=> false);

    }else{
      String message = response.error!.message;
          if(response.error!.statusCode == -1){
            message = "Bad Network";
          }else if(response.error!.statusCode == 403){
            message = "Invalid password";
          }else if(response.error!.statusCode == 404){
            message = "User not found";
          }
          Dialogs.alert(context, title: "ERROR", description: message);
    }
    }
  }

  @override
  Widget build(BuildContext context) {
    final Responseive responseive = Responseive.of(context);
    return  Positioned(
      bottom: 30,
      child: Container(
        constraints: BoxConstraints(maxWidth: responseive.isTablet?430:360,),
        child: Form(
          key: _formKey,
          child: Column(
          children: <Widget>[
             InputText(
              keyboardType: TextInputType.emailAddress,
              label: "EMAIL ADDRESS",
              fontSize: responseive.dp(responseive.isTablet?1.2:1.4),
              onChanged: (text){
                _email = text;
              },
              validator: (text){
                if(!text!.contains("@")){
                  return "Invalid email";
                }
                return null;
              },
              ),
              SizedBox(
              height: responseive.dp(2),
             ),
             Container(
              decoration: const BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.black12
                    )
                  ),
              ),
              child: Row(
                children: <Widget>[
               Expanded(
                child: InputText(
              label: "PASSWORD",
              obscureText: true,
              borderEnabled: false,
               onChanged: (text){
                _password = text;
              },
              validator: (text){
                if(text!.trim().isEmpty){
                  return "Invalid password";
                }
                return null;
                
              },
              fontSize: responseive.dp(responseive.isTablet?1.2:1.4),
              ),
              ),
              TextButton(
                child:   Text(
                  "Forgot Password",
                style: 
                TextStyle(
                  fontWeight: FontWeight.bold
                  ,fontSize: responseive.dp(responseive.isTablet?1.2:1.5),
                  )
                  ),
                onPressed: () {
                
              },
                )
                
             ],
             ),
             ),
              SizedBox(
              height: responseive.dp(5),
             ),
             Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(vertical: 5),
              color: Colors.pinkAccent,
               child: TextButton(
                 child:  Text("Sign in", style: TextStyle(color: Colors.white,
                 fontSize: responseive.dp(1.5)),),
                 onPressed: _submit,
               ),
             ),
              SizedBox(
              height: responseive.dp(2),
             ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
              children:<Widget>[
                  Text("New to friendly Desi?", 
                 style: TextStyle( fontSize:responseive.dp(1.5) ),),
                 TextButton(onPressed: () {
                   Navigator.pushNamed(context, 'register');
                 }, 
                 child: Text(
                    "Sign up", 
                 style: TextStyle(
                  color: Colors.pinkAccent
                  , fontSize:responseive.dp(1.5)),
                  )
                  
                  )
          
              ],
            ),
            SizedBox(
              height: responseive.dp(10),
             ),
          ],
              ),
        ),
      ),
    );
  }
}