import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class AvatarButton extends StatelessWidget {
  final double ImageSize;
  const AvatarButton({super.key, this.ImageSize = 100});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.all(10),
          decoration: const BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                    blurRadius: 20,
                    color: Colors.black26,
                    offset: Offset(0, 20))
              ]),
          child: ClipOval(
            child: Image.network(
              'https://www.w3schools.com/howto/img_avatar.png',
              width: ImageSize,
              height: ImageSize,
            ),
          ),
        ),
        Positioned(
          bottom: 5,
          right: 0,
          child: CupertinoButton(
            padding: const EdgeInsets.only(),
            borderRadius: BorderRadius.circular(30),
            child: Container(
              padding: const EdgeInsets.all(3),
              decoration: BoxDecoration(
                  color: Colors.pink,
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.white, width: 2)),
              child: const Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
            onPressed: () {},
          ),
        ),
      ],
    );
  }
}
