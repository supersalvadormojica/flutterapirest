
import 'package:flutter/material.dart';
import 'package:flutter_api_rest/API/authentication_api.dart';
import 'package:flutter_api_rest/Utils/dialogs.dart';
import 'package:flutter_api_rest/Utils/responsive.dart';
import 'package:flutter_api_rest/data/authentication_client.dart';
import 'package:flutter_api_rest/pages/home_page.dart';
import 'package:flutter_api_rest/widgets/input_text.dart';
import 'package:get_it/get_it.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({super.key});

  @override
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _authenticationAPI = GetIt.instance<AuthenticationAPI>();
  final _authenticationClient = GetIt.instance<AuthenticationClient>();
  final GlobalKey<FormState> _formKey = GlobalKey();
  String _email='', _password='', _username = "";
  
  Future<void> _submit() async {
    final bool? isOk = _formKey.currentState?.validate();
    if(isOk!){
      ProgressDialog.show(context);
    final  response = await _authenticationAPI.register(
        username: _username, 
        email: _email, 
        password: _password
        );
        ProgressDialog.dissmiss(context);
        if(response.data != null){
           await _authenticationClient.saveSession(response.data);
          Navigator.pushNamedAndRemoveUntil(context, HomePage.routeName, (_)=> false);

        } else{
          String message = response.error!.message;
          if(response.error!.statusCode == -1){
            message = "Bad Network";
          }else if(response.error!.statusCode == 400){
            message = "Duplicated user";
          }
          Dialogs.alert(context, title: "ERROR", description: message);
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    final Responseive responseive = Responseive.of(context);
    return  Positioned(
      bottom: 30,
      child: Container(
        constraints: BoxConstraints(maxWidth: responseive.isTablet?430:360,),
        child: Form(
          key: _formKey,
          child: Column(
          children: <Widget>[
             InputText(
              keyboardType: TextInputType.emailAddress,
              label: "USERNAME",
              fontSize: responseive.dp(responseive.isTablet?1.2:1.4),
              onChanged: (text){
                _username = text;
              },
              validator: (text){
                if(text!.trim().length<5){
                  return "Invalid username";
                }
                return null;
              },
              ),
              SizedBox(
              height: responseive.dp(2),
             ),
              InputText(
              keyboardType: TextInputType.emailAddress,
              label: "EMAIL ADDRESS",
              fontSize: responseive.dp(responseive.isTablet?1.2:1.4),
              onChanged: (text){
                _email = text;
              },
              validator: (text){
                if(!text!.contains("@")){
                  return "Invalid email";
                }
                return null;
              },
              ),

              SizedBox(
              height: responseive.dp(2),
             ),
              InputText(
              keyboardType: TextInputType.emailAddress,
              label: "PASSWORD",
              obscureText: true,
              fontSize: responseive.dp(responseive.isTablet?1.2:1.4),
              onChanged: (text){
                _password = text;
              },
              validator: (text){
                if(text!.trim().length < 6){
                  return "Invalid password";
                }
                return null;
              },
              ),
              SizedBox(
              height: responseive.dp(5),
             ),
             Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(vertical: 5),
              color: Colors.pinkAccent,
               child: TextButton(
                 child:  Text("Sign up", style: TextStyle(color: Colors.white,
                 fontSize: responseive.dp(1.5)),),
                 onPressed: _submit,
               ),
             ),
              SizedBox(
              height: responseive.dp(2),
             ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
              children:<Widget>[
                  Text("Already have an account?", 
                 style: TextStyle( fontSize:responseive.dp(1.5) ),),
                 TextButton(onPressed: () {
                   Navigator.pop(context);
                 }, 
                 child: Text(
                    "Sign in", 
                 style: TextStyle(
                  color: Colors.pinkAccent
                  , fontSize:responseive.dp(1.5)),
                  )
                  
                  )
          
              ],
            ),
            SizedBox(
              height: responseive.dp(10),
             ),
          ],
              ),
        ),
      ),
    );
  }
}