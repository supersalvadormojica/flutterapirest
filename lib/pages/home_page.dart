import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_api_rest/API/account_api.dart';
import 'package:flutter_api_rest/data/authentication_client.dart';
import 'package:flutter_api_rest/models/user.dart';
import 'package:flutter_api_rest/pages/login_page.dart';
import 'package:flutter_api_rest/pages/logs.dart';
import 'package:get_it/get_it.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;

class  HomePage extends StatefulWidget {
  static const routeName = "home";
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final  _authenticationClient = GetIt.instance<AuthenticationClient>();
  final _accountAPI = GetIt.instance<AccountAPI>();
  User? _user;

  @override
  void initState() {
    super.initState();

    

    WidgetsBinding.instance.addPostFrameCallback((_){
      _loadUser();
    });
  }

  Future<void> _loadUser() async {
    _accountAPI.getUserInfo();
   final response = await _accountAPI.getUserInfo();
   if(response != null){
    _user = response.data;
    setState(() {
      
    });
   }
  }

  Future<void> _signOut() async {
           await _authenticationClient.singOut();
           Navigator.pushNamedAndRemoveUntil(context, LoginPage.routeName, (_) => false);
          }

  Future<void> _pickImage() async {
    final ImagePicker imagePicker = ImagePicker();
    final pickedfile = await imagePicker.pickImage(source: ImageSource.camera);
    if(pickedfile != null){
      final bytes = await pickedfile.readAsBytes();
      final filename = path.basename(pickedfile.path);
      final response = await _accountAPI.updateAvatar(bytes, filename);
      if(response?.data != null){
        _user = _user!.copyWith(avatar: response?.data);
        final String imageUrl = "http://192.168.1.72:8080${response?.data}";
        Logs.p.i(imageUrl);
        setState(() {
          
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if(_user == null) CircularProgressIndicator(),
            if(_user != null) Column(children: [
              if(_user?.avatar != null)
              ClipOval(
              child:  Image.network("http://192.168.1.72:8080${_user?.avatar}",
              width: 100,
              height: 100,
              fit: BoxFit.cover,
              ),
              ),
              Text(_user!.id),
              Text(_user!.username),
              Text(_user!.email),
              Text(_user!.createdAt.toIso8601String()
              ),
              
            ],),
            SizedBox(height: 30,),
               TextButton(onPressed: _pickImage,
           child: Text("Update avatar")
           ),
            
            TextButton(onPressed: _signOut,
           child: Text("Sign Out")
           )
           ]
        ),
      )
    );
  }
}