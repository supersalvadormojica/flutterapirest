import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_api_rest/Utils/responsive.dart';
import 'package:flutter_api_rest/widgets/avatar_button.dart';
import 'package:flutter_api_rest/widgets/circle.dart';
import 'package:flutter_api_rest/widgets/icon_container.dart';
import 'package:flutter_api_rest/widgets/register_form.dart';


class RegisterPage extends StatefulWidget {
  static const routeName = 'register';
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    final Responseive responseive = Responseive.of(context);
    
    final double pinkSize = responseive.wp(88);
     final double orangeSize = responseive.wp(57);
    return Scaffold(
      body: GestureDetector(
        onTap: (){
          FocusScope.of(context).unfocus();
        },
        child: SingleChildScrollView(
          child: Container(
          width: double.infinity,
          height:responseive.height,
          color: Colors.white,
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
                Positioned(
                top: -pinkSize*0.3,
                right: -pinkSize*0.2,
                child:Circle(
                size: pinkSize,
                colors: [
                  Colors.pinkAccent,
                Colors.pink],
                )
                ),
                 Positioned(
                top: -orangeSize * 0.35,
                left: -orangeSize * 0.15,
                child:Circle(
                size: orangeSize,
                colors: [
                  Colors.orange,
                Colors.deepOrangeAccent],
                )
                ),
                Positioned(
                  top: pinkSize * .22,
        
                  child: Column(
                    children: <Widget>[
                      Text("Hello!\nSign up to get started.",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: responseive.dp(1.6),
                      ),
                      ),
                      SizedBox(
                        height:responseive.dp(4.5),
                      ),
                      AvatarButton(
                        ImageSize: responseive.wp(25),
                      )
                    ],
                  )
                  ),
                  RegisterForm(),
              Positioned(
                left: 15,
                top: 15,
                child: SafeArea(
                  child: CupertinoButton(
                    color: Colors.black26,
                    padding: const EdgeInsets.all(16),
                    borderRadius: BorderRadius.circular(30),
                    child: const Icon(Icons.arrow_back), onPressed: () {
                      Navigator.pop(context);
                  },
                  ),
                )
              
              )
                  
            ],
          ),
        ),),
      ),

    );
  }
}